# Spring Boot High Chart Graph API Example

Run this project by this command : `mvn clean spring-boot:run`

### Screenshot

Home Page

![Home Page](img/home.png "Home Page")

Activity Chart Page

![Activity Chart Page](img/activityChart.png "Activity Chart Page")

Graph1 Chart Page

![Graph1 Chart Page](img/graph1.png "Graph1 Chart Page")

Graph2 Chart Page

![Graph2 Chart Page](img/graph2.png "Graph2 Chart Page")

Line Chart Page

![Line Chart Page](img/lineChart.png "Line Chart Page")

Pie Chart Page

![Pie Chart Page](img/pie.png "Pie Chart Page")