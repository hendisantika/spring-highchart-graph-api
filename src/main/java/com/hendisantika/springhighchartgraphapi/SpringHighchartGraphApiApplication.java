package com.hendisantika.springhighchartgraphapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringHighchartGraphApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringHighchartGraphApiApplication.class, args);
    }
}
