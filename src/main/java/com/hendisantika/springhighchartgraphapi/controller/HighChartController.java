package com.hendisantika.springhighchartgraphapi.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-highchart-graph-api
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2018-12-01
 * Time: 17:29
 * To change this template use File | Settings | File Templates.
 */
@Controller
public class HighChartController {

    @GetMapping("/")
    public String index() {
        return "index";
    }

    @RequestMapping("/graph1")
    public String generateGraph(Model model) {
        Map<String, Integer> techMap = getTechnologyMap();
        model.addAttribute("techMap", techMap);
        return "graph1";
    }

    @RequestMapping("/graph2")
    public String generateRotateGraph(Model model) {
        Map<String, Integer> techMap = getTechnologyMap();
        model.addAttribute("techMap", techMap);
        return "graph2";
    }

    @RequestMapping("/lineChart")
    public String lineXhartGraph(Model model) {
        Map<String, Integer> techMap = getTechnologyMap();
        model.addAttribute("techMap", techMap);
        return "lineChart";
    }

    @RequestMapping("/pie")
    public String generatePieChart(Model model) {
        model.addAttribute("Java", 48);
        model.addAttribute("Net", 12);
        model.addAttribute("python", 9);
        model.addAttribute("Rpa", 21);
        model.addAttribute("MainFrame", 10);
        return "pie";
    }

    @RequestMapping("/activityChart")
    public String generateActivityChart(Model model) {
        model.addAttribute("java", 80);
        model.addAttribute("Net", 40);
        model.addAttribute("python", 60);
        model.addAttribute("Rpa", 90);
        model.addAttribute("MainFrame", 10);
        return "activity";
    }

    private Map<String, Integer> getTechnologyMap() {
        Map<String, Integer> techMap = new ConcurrentHashMap<>();
        techMap.put("Java", 89);
        techMap.put(".Net", 57);
        techMap.put("Php", 65);
        techMap.put("Python", 45);
        techMap.put("SAP", 78);
        techMap.put("Pega", 32);
        techMap.put("RPA", 92);
        techMap.put("Pega", 29);
        return techMap;
    }
}