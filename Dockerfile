FROM amazoncorretto:21-alpine-jdk
LABEL maintainer="hendisantika@yahoo.co.id"
VOLUME /tmp
EXPOSE 9000
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} highchart-1.0.0-RELEASE.jar
ENTRYPOINT ["java","-jar","highchart-1.0.0-RELEASE.jar"]
